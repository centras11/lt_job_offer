Job Request app

App requires additional vendors to run: Twig template engine and Doctrine ORM for working with data base.
All vendors can be installed using composer 'composer install'.

Edit MySql credentials on /config/config.php
MySql db schema can be created using 'vendor/bin/doctrine orm:schema-tool:update --force' command or can be imported (with included test data) from file /config/db_schema.sql.

Navigate to public/index.php to start

In test data two users can be found:
Email            | Passs   |Role
admin@app.com      admin    admin
user@app.com       user     user

Some test examples are done using phpspec. Test can be run using command: vendor/bin/phpspec run

ToDo list:
* decrypt user passwords - currently stored as plain text
* add some mailer (as Mandril or SendGrid) for sending emails
* improve form validation adding posted data check - must be sure email is valid email, working hours integer from 1 to 8 and similar
* cover all code with tests
* as admin of the app I would like to see report based on date, user and office
* as app user I would like to request not only single days but weeks also
* as app user I would like to inform admin I got sick
