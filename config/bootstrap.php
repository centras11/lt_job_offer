<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\EventManager;
use App\Event\GenerateUserPasswordOnCreateEventSubscriber;
use App\Event\SendJobConfirmationEventSubscriber;
use App\Event\SendUserConfirmationOnCreateEventSubscriber;

require_once __DIR__ . '/../vendor/autoload.php';
require_once 'config.php';

$paths = [__DIR__ . "/../app"];

$dbParams = [
    'dbname'   => DB_NAME,
    'user'     => DB_USER,
    'password' => DB_PASS,
    'host'     => DB_HOST,
    'driver'   => 'pdo_mysql',
];

$evm = new EventManager();

$evm->addEventSubscriber(new SendJobConfirmationEventSubscriber);
$evm->addEventSubscriber(new GenerateUserPasswordOnCreateEventSubscriber);
$evm->addEventSubscriber(new SendUserConfirmationOnCreateEventSubscriber);


$isDevMode = TRUE;
$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$entityManager = EntityManager::create($dbParams, $config, $evm);
