-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 24, 2019 at 05:45 PM
-- Server version: 5.7.25-0ubuntu0.16.04.2
-- PHP Version: 7.2.16-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lt_job_offer`
--

-- --------------------------------------------------------

--
-- Table structure for table `job_request`
--

CREATE TABLE `job_request` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `office` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) DEFAULT NULL,
  `job_day` date DEFAULT NULL,
  `hours` smallint(6) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `job_request`
--

INSERT INTO `job_request` (`id`, `user_id`, `office`, `status`, `job_day`, `hours`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'office 1', 2, '2019-03-26', 8, '2019-03-24 16:12:33', '2019-03-24 16:14:23'),
(2, 2, 'home', 3, '2019-03-26', 5, '2019-03-24 16:13:02', '2019-03-24 16:14:28'),
(3, 2, 'office 1', 1, '2019-03-27', 8, '2019-03-24 16:13:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role` smallint(6) NOT NULL,
  `email_access_granted` tinyint(1) NOT NULL,
  `git_repository_granted` tinyint(1) NOT NULL,
  `microsoft_office_licence` tinyint(1) NOT NULL,
  `trello_access_granted` tinyint(1) NOT NULL,
  `status` smallint(6) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `role`, `email_access_granted`, `git_repository_granted`, `microsoft_office_licence`, `trello_access_granted`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 'App Admin', 'admin@app.com', 'admin', 2, 0, 0, 0, 0, 1, '2019-03-24 00:00:00', NULL),
(2, 'App User', 'user@app.com', 'user', 1, 0, 0, 0, 0, 1, '2019-03-24 16:11:34', '2019-03-24 16:11:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `job_request`
--
ALTER TABLE `job_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_A1783804A76ED395` (`user_id`),
  ADD KEY `status_idx` (`status`),
  ADD KEY `office_idx` (`office`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_idx` (`status`),
  ADD KEY `email_idx` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `job_request`
--
ALTER TABLE `job_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `job_request`
--
ALTER TABLE `job_request`
  ADD CONSTRAINT `FK_A1783804A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
