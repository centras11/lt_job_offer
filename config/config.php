<?php
// Database Params
define('DB_HOST','localhost');
define('DB_USER','root');
define('DB_PASS','admin');
define('DB_NAME','lt_job_offer');
define('DB_CHARSET','utf8');


define('ROOT', dirname(dirname(__FILE__)));
define('APP', ROOT . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR);
define('VIEW', APP . 'View' . DIRECTORY_SEPARATOR);
define('CONTROLLER_NAMESPACE', 'App\Controller\\');
