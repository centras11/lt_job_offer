<?php

namespace App\Controller;

use App\Core\Controller;
use App\Core\View;
use App\Entity\Filter\JobRequestFilter;
use App\Model\JobRequestModel;

class JobRequestController extends Controller
{
    /**
     * @var View
     */
    protected $view;

    /**
     * @var JobRequestModel
     */
    protected $model;

    public function __construct()
    {
        parent::__construct(new View(), new JobRequestModel());
    }

    // show job requests list action
    public function index()
    {
        $filter = new JobRequestFilter();

        if (count($_POST) > 0) {
            $filter = $this->model->createFilterFromPost($_POST);
        }

        $data = [
            'list' => $this->model->getList($filter),
        ];

        $this->view->render('JobRequest' . DIRECTORY_SEPARATOR . 'index.html.twig', $data);
    }

    // changes JobRequest status
    /**
     * @param  integer $jobRequest
     * @param  integer $status
     */
    public function changeStatus($jobRequest, $status)
    {
        $this->model->changeStatus($jobRequest, $status);

        $data = [
            'success' => true,
            'id' => $jobRequest,
            'statusMessage' => $this->model->getStatusMessage($status),
        ];

        echo(json_encode($data));
    }

    // creates new JobRequest using $_POST data from form
    public function create()
    {
        if (count($_POST) > 0) {
            $this->model->createNewFromPost($_SESSION['userId'], $_POST);
            $url = '/index.php/JobRequest/index';
            $this->redirectToUrl($url);
        }

        $data = [
            'minStartDay' => $this->model->getMinStartDay(),
        ];

        $this->view->render('JobRequest' . DIRECTORY_SEPARATOR . 'create.html.twig', $data);
    }
}