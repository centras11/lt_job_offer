<?php

namespace App\Controller;

use App\Core\Controller;
use App\Core\View;
use App\Entity\Filter\UserFilter;
use App\Entity\User;
use App\Model\UserModel;

class UserController extends Controller
{
    /**
     * @var View
     */
    protected $view;

    /**
     * @var UserModel
     */
    protected $model;

    public function __construct()
    {
        parent::__construct(new View(), new UserModel());
    }

    // show user list action
    public function index()
    {
        $filter = new UserFilter();

        if (count($_POST) > 0) {
            $filter = $this->model->createFilterFromPost($_POST);
        }

        $data = [
            'list' => $this->model->getList($filter),
        ];

        $this->view->render('User' . DIRECTORY_SEPARATOR . 'index.html.twig', $data);
    }

    // login
    public function login()
    {
        if (count($_POST) > 0) {
            $user = $this->model->login($_POST);

            if (null != $user) {
                $url = '/index.php/JobRequest/index';
                $this->redirectToUrl($url);
            }
        }

        $this->view->render('User' . DIRECTORY_SEPARATOR . 'login.html.twig');
    }

    // logout
    public function logout()
    {
        session_destroy();
        $url = '/index.php/User/login';

        $this->redirectToUrl($url);
    }

    /**
     * @param  int     $user
     * @param  string  $grant
     * @param  string  $action
     */
    public function changeGrantStatus($user, $grant, $action = 'grant')
    {
        $this->model->changeGrantStatus($user, $grant, $action);
        $url = '/index.php/User/index';

        $this->redirectToUrl($url);
    }

    /**
     * @param  integer $user
     * @param  integer $status
     */
    public function changeStatus($user, $status)
    {
        $this->model->changeStatus($user, $status);
        $url = '/index.php/User/index';
        $this->redirectToUrl($url);
    }

    // edit or create user
    /**
     * @param  integer $userId
     */
    public function edit($userId = null)
    {
        $user = new User();

        if (null != $userId) {
            $user = $this->model->getOneById($userId);
        }

        if (count($_POST) > 0) {
            $this->model->createNewFromPost($user, $_POST);
            $url = '/index.php/User/index';
            $this->redirectToUrl($url);
        }

        $data = [
            'user' => $user
        ];

        $this->view->render('User' . DIRECTORY_SEPARATOR . 'edit.html.twig', $data);
    }
}