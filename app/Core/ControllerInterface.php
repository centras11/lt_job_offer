<?php

namespace App\Core;


Interface ControllerInterface
{
    /**
     * @return ViewInterface
     */
    public function getView();

    /**
     * @param ViewInterface $view
     */
    public function setView($view);

    /**
     * @return ModelInterface
     */
    public function getModel();

    /**
     * @param ModelInterface $model
     */
    public function setModel($model);

}