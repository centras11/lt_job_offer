<?php

namespace App\Core;

// main controller
// gets two variables on creation: view and model

class Controller implements ControllerInterface
{
    /**
     * @var ViewInterface
     */
    protected $view;

    /**
     * @var ModelInterface
     */
    protected $model;

    /**
     * @param ViewInterface  $view
     * @param ModelInterface $model
     */
    public function __construct(ViewInterface  $view, ModelInterface $model)
    {
        $this->ifLoggedIn();
        $this->setView($view);
        $this->setModel($model);
    }

    /**
     * {@inheritdoc}
     */
    public function getView(){

        return $this->view;
    }

    /**
     * {@inheritdoc}
     */
    public function setView($view)
    {
        $this->view = $view;
    }

    /**
     * {@inheritdoc}
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * {@inheritdoc}
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @param string $url
     */
    public function redirectToUrl($url)
    {
        header('Location: ' . $url, true, 301);

        exit();
    }

    private function ifLoggedIn()
    {
        // checks if is saved user data on session
        if (isset($_SESSION['userId'])) {

            return;
        }

        // allows to reach login page without user logged in user
        if ($_SERVER['REQUEST_URI'] == '/index.php/User/login') {

            return;
        }

        if ($_SERVER['REQUEST_URI'] == '/index.php') {

            return;
        }

        $url = '/index.php';
        $this->redirectToUrl($url);
    }
}