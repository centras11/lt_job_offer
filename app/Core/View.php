<?php

namespace App\Core;

use Twig\Environment as Twig;

class View implements ViewInterface
{
    /**
     * @var Twig
     */
    protected $twig;

    public function __construct()
    {
        $loader = new \Twig_Loader_Filesystem(VIEW);
        $twig = new \Twig_Environment($loader);
        $twig->addGlobal("session", $_SESSION);
        $this->setTwig($twig);
    }

    /**
     * @param string $templateName
     * @param array  $data
     */
    public function render($templateName, $data = [])
    {
        echo $this->getTwig()->render($templateName, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getTwig()
    {
        return $this->twig;
    }

    /**
     * {@inheritdoc}
     */
    public function setTwig($twig)
    {
        $this->twig = $twig;
    }
}