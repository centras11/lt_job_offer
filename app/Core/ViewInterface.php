<?php

namespace App\Core;

use Twig\Environment as Twig;

interface ViewInterface
{
    /**
     * @param string $templateName
     * @param array  $data
     */
    public function render($templateName, $data = []);

    /**
     * @return Twig
     */
    public function getTwig();

    /**
     * @param Twig $twig
     */
    public function setTwig($twig);
}