<?php

namespace App\Core;

use Doctrine\ORM\EntityManager;

class Model implements ModelInterface
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    public function __construct()
    {
        $entityManager = '';
        require_once __DIR__ . '/../../config/bootstrap.php';

        $this->setEntityManager($entityManager);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }
}