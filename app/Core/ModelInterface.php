<?php

namespace App\Core;

use Doctrine\ORM\EntityManager;

interface ModelInterface
{
    /**
     * @return EntityManager
     */
    public function getEntityManager();

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager($entityManager);

}