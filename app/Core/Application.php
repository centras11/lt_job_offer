<?php

namespace App\Core;

// Works like router
// index.php/{controller}/{action}/{some}/{parameters}

class Application
{
    /**
     * @var object
     */
    private $controller;

    /**
     * @var string
     */
    private $controllerName = CONTROLLER_NAMESPACE . 'UserController';

    /**
     * @var string
     */
    private $action = 'login';

    /**
     * @var array
     */
    private $params = [];

    public function __construct()
    {
        $this->prepareUrl();
        $this->createController($this->getControllerName());
        call_user_func_array([$this->getController(),$this->action], $this->params);
    }

    protected function prepareUrl()
    {
        $request = trim($_SERVER['REQUEST_URI'], '/');

        if (!empty($request)) {
            $url = explode('/', $request);

            if (isset($url[1]) && !empty($url[1])) {
                $this->setControllerName($url[1]);
            }

            if (isset($url[2]) && !empty($url[2])) {
                $this->setAction($url[2]);
            }

            unset($url[0], $url[1], $url[2]);
            $this->setParams(array_values($url));
        }
    }

    /**
     * @return object
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param object $controller
     */
    public function setController($controller)
    {
        $this->controller = $controller;
    }

    /**
     * @param string $controllerName
     */
    public function createController($controllerName)
    {
        $this->controller = new $controllerName();
    }

    /**
     * @return string
     */
    public function getControllerName()
    {
        return $this->controllerName;
    }

    /**
     * @param string $controllerName
     */
    public function setControllerName($controllerName)
    {
        $this->controllerName = $this->getFullControllerName($controllerName);
    }

    /**
     * @param string $controllerName
     *
     * @return string
     */
    private function getFullControllerName($controllerName)
    {
        return CONTROLLER_NAMESPACE . $controllerName . 'Controller';
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param array $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }
}