<?php

namespace App\Model;

use App\Core\Model;
use App\Entity\JobRequest;
use App\Entity\Repository\JobRequestRepository;
use App\Entity\Filter\JobRequestFilter;
use App\Entity\User;

class JobRequestModel extends Model
{
    /**
     * @param array $post
     *
     * @return JobRequestFilter
     */
    public function createFilterFromPost($post)
    {
        $filter = new JobRequestFilter();

        foreach ($post as $key => $value)  {

            if ('' == $value) {

                continue;
            }

            switch ($key) {
                case 'email':
                    $filter->setEmail($value);
                    break;
                case 'status':
                    $filter->setStatus($value);
                    break;
                case 'office':
                    $filter->setOffice($value);
                    break;
            }
        }

        return $filter;
    }

    /**
     * @param JobRequestFilter $filter
     *
     * @return array
     */
    public function getList(JobRequestFilter $filter)
    {
        // if user not admin - showing only user's job requests
        if ($_SESSION['userRole'] == User::ROLE_USER) {
            $filter->setUserId($_SESSION['userId']);
        }

        $list = $this->getRepository()->findByFilter($filter);

        return $list;
    }

    /**
     * @param integer $id
     *
     * @return JobRequest|null
     */
    public function getOneById($id)
    {
        $entity = $this->getRepository()->findOneBy(['id' => $id]);

        if ($entity instanceof JobRequest) {

            return $entity;
        }

        return null;
    }

    /**
     * @param integer $id
     * @param integer $user
     *
     * @return JobRequest|null
     */
    public function getOneByIdAndUser($id, $user)
    {
        $entity = $this->getOneById($id);

        if (null == $entity || $entity->getUser()->getId() != $user) {

            return null;
        }

        return $entity;
    }

    /**
     * @param integer $jobRequestId
     * @param integer $status
     *
     * @return boolean
     */
    public function changeStatus($jobRequestId, $status)
    {
        $jobRequest = $this->getRepository()->findOneBy(['id' => $jobRequestId]);

        if ($jobRequest instanceof JobRequest) {
            $jobRequest->setStatus($status);
            $this->update($jobRequest);
        }

        return true;
    }

    /**
     * @param JobRequest $entity
     * @param boolean    $flush
     */
    private function update(JobRequest $entity, $flush = true)
    {
        $this->getRepository()->update($entity, $flush);
    }

    /**
     * @param integer $status
     *
     * @return string
     */
    public function getStatusMessage($status)
    {
        switch ($status) {
            case JobRequest::STATUS_APPROVED:
                $message = '<span class="badge badge-success">Approved</span>';
                break;
            case JobRequest::STATUS_REJECTED:
                $message = '<span class="badge badge-danger">Rejected</span>';
                break;
            default:
                $message = '-';
        }

        return $message;
    }

    /**
     * @return string
     */
    public function getMinStartDay()
    {
        $start = \DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));

        // removing 8 hr from current time if user is sick, adding 8 hr if active user
        if ($_SESSION['userStatus'] == User::STATUS_SICK) {
            $start->sub(new \DateInterval('PT8H'));
        } else {
            $start->add(new \DateInterval('PT8H'));
        }

        $start->add(new \DateInterval('P1D'));

        return $start->format('Y-m-d');
    }

    /**
     * @param integer $userId
     * @param array   $post
     *
     * @return JobRequest
     */
    public function createNewFromPost($userId, $post)
    {
        $user = $this->getEntityManager()->getReference('App\Entity\User', $userId);

        // if sick user making request he is marked as active
        if ($_SESSION['userStatus'] == User::STATUS_SICK) {
            $user->setStatus(User::STATUS_ACTIVE);
        }

        // allow users from office work full day only
        if ($post['office'] != 'home') {
            $post['hours'] = 8;
        }

        $jobRequest = new JobRequest();
        $jobRequest->setHours($post['hours']);
        $jobRequest->setJobDay(\DateTime::createFromFormat('Y-m-d', $post['jobDay']));
        $jobRequest->setOffice($post['office']);
        $jobRequest->setUser($user);
        $this->update($jobRequest);

        return $jobRequest;
    }

    /**
     * @return JobRequestRepository
     */
    private function getRepository()
    {
        return $this->getEntityManager()->getRepository(JobRequest::class);
    }



}