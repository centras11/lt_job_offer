<?php

namespace App\Model;

use App\Core\Model;
use App\Entity\User;
use App\Entity\Repository\UserRepository;
use App\Entity\Filter\UserFilter;

class UserModel extends Model
{
    /**
     * @param array $post
     *
     * @return UserFilter
     */
    public function createFilterFromPost($post)
    {
        $filter = new UserFilter();

        foreach ($post as $key => $value)  {

            if ('' == $value) {

                continue;
            }

            switch ($key) {
                case 'email':
                    $filter->setEmail($value);
                    break;
                case 'status':
                    $filter->setStatus($value);
                    break;
            }
        }

        return $filter;
    }

    /**
     * @param UserFilter $filter
     *
     * @return array
     */
    public function getList(UserFilter $filter)
    {
        $list = $this->getRepository()->findByFilter($filter);

        return $list;
    }

    /**
     * @param integer $id
     *
     * @return User|null
     */
    public function getOneById($id)
    {
        $user = $this->getRepository()->findOneBy(['id' => $id]);

        if ($user instanceof User) {

            return $user;
        }

        return null;
    }

    /**
     * @param integer $userId
     * @param integer $status
     *
     * @return boolean
     */
    public function changeStatus($userId, $status)
    {
        $user = $this->getRepository()->findOneBy(['id' => $userId]);

        if (! $user instanceof User) {

            return false;
        }

        $user->setStatus($status);
        $this->update($user);

        return true;
    }

    /**
     * @param User  $user
     * @param array $post
     *
     * @return User
     */
    public function createNewFromPost(User $user, $post)
    {
        $user->setEmail($post['email']);
        $user->setName($post['name']);
        $user->setRole($post['role']);
        $this->update($user);

        return $user;
    }

    /**
     * @param array   $post
     *
     * @return User|null
     */
    public function login($post)
    {
        //@todo extra check if $post['email'] is email, if password is alphanumeric
        $user = $this->getRepository()->findOneBy(['email' => $post['email'], 'password' => $post['password']]);

        if (! $user instanceof User) {

            return null;
        }

        if ($user->getStatus() == User::STATUS_LOCKED) {

            return null;
        }

        $this->setSession($user);

        return $user;
    }

    /**
     * @param User $user
     */
    private function setSession(User $user)
    {
        $_SESSION['userId'] = $user->getId();
        $_SESSION['userRole'] = $user->getRole();
        $_SESSION['userStatus'] = $user->getStatus();
        $_SESSION['userName'] = $user->getName();
    }


    /**
     * @param integer $userId
     * @param string  $grant
     * @param integer $action
     *
     * @return boolean
     */
    public function changeGrantStatus($userId, $grant, $action)
    {
        $user = $this->getOneById($userId);

        if (null == $user) {

            return false;
        }

        $change = true;

        if ($action != 'grant') {
            $change = false;
        }

        switch ($grant) {
            case 'email':
                $user->setEmailAccessGranted($change);
                break;
            case 'git':
                $user->setGitRepositoryGranted($change);
                break;
            case 'msLicence':
                $user->setMicrosoftOfficeLicence($change);
                break;
            case 'trello':
                $user->seTtrelloAccessGranted($change);
                break;
        }

        $this->update($user);

        return true;
    }

    /**
     * @param User    $entity
     * @param boolean $flush
     */
    private function update(User $entity, $flush = true)
    {
        $this->getRepository()->update($entity, $flush);
    }

    /**
     * @return UserRepository
     */
    private function getRepository()
    {
        return $this->getEntityManager()->getRepository(User::class);
    }



}