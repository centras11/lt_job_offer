<?php

namespace App\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use App\Entity\JobRequest;
use App\Entity\Filter\JobRequestFilter;

/**
 * JobRequestRepository custom repository
 */
class JobRequestRepository extends EntityRepository
{
    /**
     * @param JobRequestFilter $filter
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getQB(JobRequestFilter $filter)
    {
        $builder = $this->getEntityManager()->createQueryBuilder();

        $builder
            ->select('jr')
            ->from('App\Entity\JobRequest', 'jr')
            ->innerJoin('jr.user', 'user');

        // filtering
        $this->applyFilter($filter, $builder);

        return $builder;
    }

    /**
     * @param JobRequestFilter           $filter
     * @param \Doctrine\ORM\QueryBuilder $builder
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function applyFilter(JobRequestFilter $filter, $builder)
    {
        if (null !== $filter->getStatus()) {
            $builder
                ->andWhere('jr.status = :status')
                ->setParameter('status', $filter->getStatus());
        }

        if (null !== $filter->getJobDay()) {
            $builder
                ->andWhere('jr.jobDay = :jobDay')
                ->setParameter('jobDay', $filter->getJobDay());
        }

        if (null !== $filter->getOffice()) {
            $builder
                ->andWhere('jr.office = :office')
                ->setParameter('office', $filter->getOffice());
        }

        if (null !== $filter->getEmail()) {
            $builder
                ->andWhere('user.email = :email')
                ->setParameter('email', $filter->getEmail());
        }

        if (null !== $filter->getUserId()) {
            $builder
                ->andWhere('user.id = :userId')
                ->setParameter('userId', $filter->getUserId());
        }

        return $builder;
    }

    /**
     * @param JobRequest $entity
     * @param boolean    $andFlush
     */
    public function update(JobRequest $entity, $andFlush = true)
    {
        $this->getEntityManager()->persist($entity);

        if ($andFlush) {
            $this->flush();
        }
    }

    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    /**
     * @param JobRequestFilter $filter
     * @param int              $limit
     *
     * @return array
     */
    public function findByFilter(JobRequestFilter $filter, $limit = 0)
    {
        $builder = $this->getQB($filter);

        if ($limit > 0) {
            $builder->setMaxResults($limit);
        }

        return $builder->getQuery()->getResult();
    }

    /**
     * @param JobRequestFilter $filter
     *
     * @return JobRequest|null
     */
    public function findOneByFilter(JobRequestFilter $filter)
    {
        $builder = $this->getQB($filter);
        $builder->setMaxResults(1);

        return $builder->getQuery()->getOneOrNullResult();
    }
}