<?php

namespace App\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use App\Entity\User;
use App\Entity\Filter\UserFilter;

/**
 * UserRepository custom repository
 */
class UserRepository extends EntityRepository
{
    /**
     * @param UserFilter $filter
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getQB(UserFilter $filter)
    {
        $builder = $this->getEntityManager()->createQueryBuilder();

        $builder
            ->select('user')
            ->from('App\Entity\user', 'user');

        // filtering
        $this->applyFilter($filter, $builder);

        return $builder;
    }

    /**
     * @param UserFilter                 $filter
     * @param \Doctrine\ORM\QueryBuilder $builder
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function applyFilter(UserFilter $filter, $builder)
    {
        if (null !== $filter->getStatus()) {
            $builder
                ->andWhere('user.status = :status')
                ->setParameter('status', $filter->getStatus());
        }

        if (null !== $filter->getEmail()) {
            $builder
                ->andWhere('user.email = :email')
                ->setParameter('email', $filter->getEmail());
        }

        return $builder;
    }

    /**
     * @param User     $entity
     * @param boolean  $andFlush
     */
    public function update(User $entity, $andFlush = true)
    {
        $this->getEntityManager()->persist($entity);

        if ($andFlush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param UserFilter $filter
     * @param int        $limit
     *
     * @return array
     */
    public function findByFilter(UserFilter $filter, $limit = 0)
    {
        $builder = $this->getQB($filter);

        if ($limit > 0) {
            $builder->setMaxResults($limit);
        }

        return $builder->getQuery()->getResult();
    }
}