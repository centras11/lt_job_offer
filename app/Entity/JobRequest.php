<?php

namespace App\Entity;

use App\Entity\Traits\StatusTrait;
use App\Entity\Traits\CreatedAtTrait;
use App\Entity\Traits\UpdatedAtTrait;

/**
 * @Entity(repositoryClass="App\Entity\Repository\JobRequestRepository")
 * @Table(
 *     name="job_request",
 *     indexes = {
 *          @Index(name="status_idx", columns={"status"}),
 *          @Index(name="office_idx", columns={"office"}),
 *     }
 * )
 * @HasLifecycleCallbacks
 */
class JobRequest
{
    use StatusTrait, CreatedAtTrait, UpdatedAtTrait;

    const STATUS_CREATED = 1;
    const STATUS_APPROVED = 2;
    const STATUS_REJECTED = 3;

    /**
     * @var integer
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="office", type="string", length=255)
     */
    private $office;

    /**
     * @var integer
     *
     * @Column(name="hours", type="smallint", nullable=true)
     */
    private $hours;

    /**
     * @var \DateTime $jobDay
     *
     * @Column(name="job_day", type="date", nullable=true)
     */
    private $jobDay;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="jobRequest")
     **/
    protected $user;

    /**
     * Constructor
     */
    public function __construct() {
        $this->status = self::STATUS_CREATED;
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the office
     *
     * @param string $office
     *
     * @return self
     */
    public function setOffice($office)
    {
        $this->office = $office;

        return $this;
    }

    /**
     * Gets office
     *
     * @return string
     */
    public function getOffice()
    {
        return $this->office;
    }

    /**
     * Sets the hours
     *
     * @param integer $hours
     *
     * @return self
     */
    public function setHours($hours)
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Gets hours
     *
     * @return integer
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Sets jobDay
     *
     * @param string $jobDay
     *
     * @return self
     */
    public function setJobDay($jobDay)
    {
        $this->jobDay = $jobDay;

        return $this;
    }

    /**
     * Gets jobDay
     *
     * @return \DateTime
     */
    public function getJobDay()
    {
        return $this->jobDay;
    }

    /**
     * Gets jobDay
     *
     * @return string
     */
    public function getJobDayAsString()
    {
        return $this->getJobDay()->format('Y-m-d');
    }

    /**
     * Sets User
     *
     * @param User $user
     *
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Gets user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }
}