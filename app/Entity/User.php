<?php

namespace App\Entity;

use App\Entity\Traits\StatusTrait;
use App\Entity\Traits\CreatedAtTrait;
use App\Entity\Traits\UpdatedAtTrait;

/**
 * @Entity(repositoryClass="App\Entity\Repository\UserRepository")
 * @Table(
 *     name="user",
 *     indexes = {
 *          @Index(name="status_idx", columns={"status"}),
 *          @Index(name="email_idx", columns={"email"}),
 *     }
 * )
 * @HasLifecycleCallbacks
 */
class User
{
    use StatusTrait, CreatedAtTrait, UpdatedAtTrait;

    const STATUS_ACTIVE = 1;
    const STATUS_LOCKED = 2;
    const STATUS_SICK = 3;

    const ROLE_USER = 1;
    const ROLE_ADMIN = 2;

    /**
     * @var integer
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * @var string
     *
     * @Column(name="password", type="string", length=100)
     */
    private $password;

    /**
     * @var integer
     *
     * @Column(name="role", type="smallint")
     */
    private $role;

    /**
     * @var boolean
     *
     * @Column(name="email_access_granted", type="boolean")
     */
    private $emailAccessGranted;

    /**
     * @var boolean
     *
     * @Column(name="git_repository_granted", type="boolean")
     */
    private $gitRepositoryGranted;

    /**
     * @var boolean
     *
     * @Column(name="microsoft_office_licence", type="boolean")
     */
    private $microsoftOfficeLicence;

    /**
     * @var boolean
     *
     * @Column(name="trello_access_granted", type="boolean")
     */
    private $trelloAccessGranted;

    /**
     * @var JobRequest[] An ArrayCollection of Bug objects.
     *
     * @OneToMany(targetEntity="JobRequest", mappedBy="user")
     **/
    protected $jobRequests;

    /**
     * Constructor
     */
    public function __construct() {
        $this->emailAccessGranted = false;
        $this->gitRepositoryGranted = false;
        $this->microsoftOfficeLicence = false;
        $this->trelloAccessGranted = false;
        $this->status = self::STATUS_ACTIVE;
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * To String
     */
    public function __toString()
    {
        return (string) $this->getName() . ' ' . $this->getEmail();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the email
     *
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Gets email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the password
     *
     * @param string $password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Gets password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set role
     *
     * @param integer $role
     *
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return integer
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Sets the emailAccessGranted
     *
     * @param boolean $emailAccessGranted
     *
     * @return self
     */
    public function setEmailAccessGranted($emailAccessGranted)
    {
        $this->emailAccessGranted = $emailAccessGranted;

        return $this;
    }

    /**
     * Gets emailAccessGranted
     *
     * @return string
     */
    public function getEmailAccessGranted()
    {
        return $this->emailAccessGranted;
    }

    /**
     * Sets the gitRepositoryGranted
     *
     * @param boolean $gitRepositoryGranted
     *
     * @return self
     */
    public function setGitRepositoryGranted($gitRepositoryGranted)
    {
        $this->gitRepositoryGranted = $gitRepositoryGranted;

        return $this;
    }

    /**
     * Gets gitRepositoryGranted
     *
     * @return string
     */
    public function getGitRepositoryGranted()
    {
        return $this->gitRepositoryGranted;
    }

    /**
     * Sets the microsoftOfficeLicence
     *
     * @param boolean $microsoftOfficeLicence
     *
     * @return self
     */
    public function setMicrosoftOfficeLicence($microsoftOfficeLicence)
    {
        $this->microsoftOfficeLicence = $microsoftOfficeLicence;

        return $this;
    }

    /**
     * Gets microsoftOfficeLicence
     *
     * @return string
     */
    public function getMicrosoftOfficeLicence()
    {
        return $this->microsoftOfficeLicence;
    }

    /**
     * Sets the trelloAccessGranted
     *
     * @param boolean $trelloAccessGranted
     *
     * @return self
     */
    public function seTtrelloAccessGranted($trelloAccessGranted)
    {
        $this->trelloAccessGranted = $trelloAccessGranted;

        return $this;
    }

    /**
     * Gets trelloAccessGranted
     *
     * @return string
     */
    public function geTtrelloAccessGranted()
    {
        return $this->trelloAccessGranted;
    }
}