<?php

namespace App\Entity\Filter;

/**
 * JobRequestFilter model
 */
class JobRequestFilter
{
    /**
     * @var int
     */
    private $status;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var string
     */
    private $jobDay;

    /**
     * @var string
     */
    private $office;

    /**
     * @var string
     */
    private $email;

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     *
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return string
     */
    public function getJobDay()
    {
        return $this->jobDay;
    }

    /**
     * @param string $jobDay
     *
     * @return $this
     */
    public function setJobDay($jobDay)
    {
        $this->jobDay = $jobDay;

        return $this;
    }

    /**
     * @return string
     */
    public function getOffice()
    {
        return $this->office;
    }

    /**
     * @param string $office
     *
     * @return $this
     */
    public function setOffice($office)
    {
        $this->office = $office;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param int $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }
}
