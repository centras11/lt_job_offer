<?php

namespace App\Entity\Traits;

trait StatusTrait
{
    /**
     * @var integer
     *
     * @Column(name="status", type="smallint", nullable=true)
     */
    private $status;

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}