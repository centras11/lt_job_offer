<?php

namespace App\Event;

use Doctrine\ORM\Events;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use App\Entity\JobRequest;
use App\Model\EmailModel;


class SendJobConfirmationEventSubscriber implements \Doctrine\Common\EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            Events::preUpdate
        ];
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof JobRequest) {

            return;
        }

        if ($args->hasChangedField('status')) {
            $this->sendConfirmation($entity);
        }
    }

    /**
     * @param JobRequest $entity
     */
    private function sendConfirmation(JobRequest $entity)
    {
        switch ($entity->getStatus()) {
            case JobRequest::STATUS_APPROVED:
                $email = $this->prepareConfirmation($entity);
            break;
            case JobRequest::STATUS_REJECTED:
                $email = $this->prepareReject($entity);
            break;
        }

        // @todo - add some mailer like Mandril or Sendgrid to send email
    }

    /**
     * @param JobRequest $entity
     *
     * @return EmailModel
     */
    private function prepareConfirmation(JobRequest $entity)
    {
        $subject = 'Job confirmation on ' . $entity->getJobDayAsString();
        $message = 'Job confirmation on ' . $entity->getJobDayAsString() . '. Some more info .....';
        $email = new EmailModel($entity->getUser()->getEmail(), $subject, $message);

        return $email;
    }

    /**
     * @param JobRequest $entity
     *
     * @return EmailModel
     */
    private function prepareReject(JobRequest $entity)
    {
        $subject = 'Job not confirmed on ' . $entity->getJobDayAsString();
        $message = 'Job confirmed on ' . $entity->getJobDayAsString() . '. Some more info .....';
        $email = new EmailModel($entity->getUser()->getEmail(), $subject, $message);

        return $email;
    }


}

