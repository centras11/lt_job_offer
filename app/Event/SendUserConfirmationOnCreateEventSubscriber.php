<?php

namespace App\Event;

use Doctrine\ORM\Events;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use App\Entity\User;
use App\Model\EmailModel;


class SendUserConfirmationOnCreateEventSubscriber implements \Doctrine\Common\EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof User) {

            return;
        }

        if (null !== $entity->getId()) {

            return;
        }

        $this->sendConfirmation($entity);
    }

    /**
     * @param User $entity
     */
    private function sendConfirmation(User $entity)
    {
        $email = $this->prepareConfirmation($entity);

        // @todo - add some mailer like Mandril or Sendgrid to send email
    }

    /**
     * @param User $entity
     *
     * @return EmailModel
     */
    private function prepareConfirmation(User $entity)
    {
        $subject = 'Welcome';
        $message = 'Sending user data: user - ' . $entity->getEmail() . ', password - ' . $entity->getPassword() . '. other information .......';
        $email = new EmailModel($entity->getEmail(), $subject, $message);

        return $email;
    }
}