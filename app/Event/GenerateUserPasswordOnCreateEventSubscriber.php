<?php

namespace App\Event;

use Doctrine\ORM\Events;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use App\Entity\User;

class GenerateUserPasswordOnCreateEventSubscriber implements \Doctrine\Common\EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof User) {

            return;
        }

        if (null !== $entity->getId()) {

            return;
        }

        $this->generateRandomPassword($entity);
    }

    /**
     * @param User $entity
     */
    private function generateRandomPassword(User $entity)
    {
        $password = md5(uniqid($entity->getEmail(), true));
        $entity->setPassword($password);
    }

}

