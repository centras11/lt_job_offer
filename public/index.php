<?php

error_reporting(-1);
ini_set('display_errors', 'On');

session_start();


use App\Core\Application;

require_once('../vendor/autoload.php');

// Load Config
require_once '../config/config.php';

new Application();
