<?php

namespace spec\App\Model;

use App\Entity\Filter\JobRequestFilter;
use App\Model\JobRequestModel;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class JobRequestModelSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(JobRequestModel::class);
    }

    function it_should_create_empty_filter()
    {
        $post = [];
        $filter = new JobRequestFilter();

        $this->createFilterFromPost($post)->shouldBeLike($filter);
    }

    function it_should_create_filter_with_office()
    {
        $post = ['office' => 'office'];
        $filter = new JobRequestFilter();
        $filter->setOffice('office');

        $this->createFilterFromPost($post)->shouldBeLike($filter);
    }

    function it_should_create_filter_with_office_status_email()
    {
        $post = ['office' => 'office', 'email' => 'email', 'status' => 1];
        $filter = new JobRequestFilter();
        $filter->setOffice('office');
        $filter->setEmail('email');
        $filter->setStatus(1);

        $this->createFilterFromPost($post)->shouldBeLike($filter);
    }
}
