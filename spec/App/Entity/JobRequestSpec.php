<?php

namespace spec\App\Entity;

use App\Entity\JobRequest;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class JobRequestSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(JobRequest::class);
    }

    function it_has_no_id_by_default()
    {
        $this->getId()->shouldReturn(null);
    }

    function it_has_createdAt_by_default()
    {
        $this->getCreatedAt()->shouldBeObject(new \DateTime('now'));
    }

    function its_created_at_is_mutable()
    {
        $createdAt = new \DateTime('now');

        $this->setCreatedAt($createdAt);
        $this->getCreatedAt()->shouldBeLike($createdAt);
    }

    function it_has_no_updatedAt_by_default()
    {
        $this->getUpdatedAt()->shouldReturn(null);
    }

    function its_updated_at_is_mutable()
    {
        $updatedAt = new \DateTime('now');

        $this->setUpdatedAt($updatedAt);
        $this->getUpdatedAt()->shouldBeLike($updatedAt);
    }
}
